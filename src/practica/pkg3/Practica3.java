package practica.pkg3;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author m_fer
 * 
 * 
 */
public class Practica3 {
    
    static Scanner sc = new Scanner(System.in);
    static Stack pila = new Stack();
    static String exp;
    static StringBuilder epos = new StringBuilder();
    static char c;
    static int hierarchy, size;
    
    public static void main(String[] args) {

        System.out.print("Inserta la expesion para pasar a infijo: ");
        exp = sc.nextLine();
        
        for (int i = 0; i < exp.length(); i++) {
            c = exp.charAt(i);
            hierarchy = getHierarchy(c);
            if(isOperator(c)){
                if(pila.isEmpty()){
                    pila.push(c);
                } else {
                    if(hierarchy == 3){
                        pila.push(c);
                    } else {
                        if (hierarchy == 4){
                            size = pila.size();
                            for (int j = 0; j < size; j++) {
                                if (pila.lastElement().equals('(') || pila.lastElement().equals(')')) {
                                    pila.pop();
                                } else {
                                    epos.append(pila.pop());
                                }
                            }
                        } else {
                            compararOperadores(c); 
                        }
                    }
                }
                
            } else {
                epos.append(c);
            }
        }
        if(!pila.isEmpty()){
            size = pila.size();
            for(int j = 0; j < size; j++) {
                if (pila.lastElement().equals('(') || pila.lastElement().equals(')')) {
                    pila.pop();
                } else {
                    epos.append(pila.pop());
                }
            }
        }
        System.out.println("\n" + epos);
        
    }

    private static boolean isOperator(char c) {
        if(c == '^' || c == '*' || c == '/' || c == '+' || c == '-' || c == '(' || c == ')'){
            return true;
        } else {
            return false;
        }
    }

    private static int getHierarchy(char c) {
        switch(c){
            case '^': 
                return 0;
            case '*': 
                return 1;
            case '/': 
                return 1;
            case '+': 
                return 2;
            case '-': 
                return 2;
            case '(': 
                return 3;
            case ')': 
                return 4;
            default: 
                return 5;
        }
    }

    private static void compararOperadores(char c) {
        if(hierarchy < getHierarchy((char)pila.lastElement())){
            pila.push(c);
        } else {
            if (pila.lastElement().equals('(') || pila.lastElement().equals(')')) {
                pila.pop();
            } else {
                epos.append(pila.pop());
            }

            if(pila.isEmpty()){
                pila.push(c);
            } else {
                compararOperadores(c);
            }
        }
    }
    
}
